/*
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package main

import (
	"fmt"
	"github.com/xanzy/go-gitlab"
	"io/ioutil"
	"os"
	"sort"
	"time"
)

type PipelineInfo struct {
	Jobs map[int]float64
}

type ProjectInfo struct {
	Pipelines map[int]*PipelineInfo
}

func main() {
	if len(os.Args) != 3 && len(os.Args) != 4 {
		fmt.Printf("syntax: %s TOKEN-FILE GROUP [PROJECT]\n", os.Args[0])
		os.Exit(1)
	}
	tokenfile := os.Args[1]
	group := os.Args[2]
	filter := ""
	if len(os.Args) == 4 {
		filter = os.Args[3]
	}
	token, _ := ioutil.ReadFile(tokenfile)

	gl, _ := gitlab.NewClient(string(token))

	listopt := gitlab.ListGroupProjectsOptions{}
	listopt.ListOptions.PerPage = 100

	projects, _, err := gl.Groups.ListGroupProjects(group, &listopt)

	if err != nil {
		fmt.Printf("List group projects %s\n", err)
	}

	paths := []string{}
	for _, project := range projects {
		paths = append(paths, project.Path)
	}
	sort.Strings(paths)

	groupdur := time.Duration(0.0)
	grouppipelines := 0
	groupjobs := 0

	infomap := make(map[string]*ProjectInfo)
	for _, path := range paths {
		fmt.Printf(".")

		proj := group + "/" + path

		info := ProjectInfo{
			Pipelines: make(map[int]*PipelineInfo),
		}
		infomap[proj] = &info

		if filter != "" && path != filter {
			continue
		}

		listpopt := gitlab.ListProjectPipelinesOptions{}
		listpopt.ListOptions.PerPage = 100
		now := time.Now()
		then := now.AddDate(0, -1, 0)
		listpopt.UpdatedAfter = &then

		for {
			fmt.Printf(".")
			pipelines, _, err := gl.Pipelines.ListProjectPipelines(proj, &listpopt)
			if err != nil {
				fmt.Printf("List pipelines %s\n", err)
			}

			//fmt.Printf("Pipelines %d\n", len(pipelines))
			for _, pipeline := range pipelines {
				//fmt.Printf("  - Pipeline %d\n", pipeline.ID)

				info.Pipelines[pipeline.ID] = &PipelineInfo{
					Jobs: make(map[int]float64),
				}
			}

			if len(pipelines) == 100 {
				listpopt.ListOptions.Page++
			} else {
				break
			}
		}

		listjopt := gitlab.ListJobsOptions{}
		listjopt.ListOptions.PerPage = 100

		for {
			fmt.Printf(".")
			jobs, _, err := gl.Jobs.ListProjectJobs(proj, &listjopt)
			if err != nil {
				fmt.Printf("List jobs %s\n", err)
			}

			//fmt.Printf("Jobs %d\n", len(jobs))
			done := false
			for _, job := range jobs {
				pipeline, ok := info.Pipelines[job.Pipeline.ID]
				if !ok {
					done = true
					break
				}
				pipeline.Jobs[job.ID] = job.Duration
			}

			if !done && len(jobs) != 0 {
				listjopt.ListOptions.Page++
			} else {
				break
			}
		}

	}

	fmt.Printf("\n\n")
	for _, path := range paths {
		proj := group + "/" + path
		info := infomap[proj]
		fmt.Printf("Project %s\n", path)
		totaldur := time.Duration(0.0)
		totaljobs := 0
		totalpipelines := 0
		for _, info := range info.Pipelines {
			totalpipelines++
			for _, duration := range info.Jobs {
				totaljobs++
				totaldur += time.Second * time.Duration(duration)
			}
		}
		fmt.Printf("   Duration: %s\n", totaldur)
		perpipeline := time.Duration(0.0)
		perjob := time.Duration(0.0)
		if totalpipelines != 0.0 {
			perpipeline = totaldur / time.Duration(totalpipelines)
		}
		if totaljobs != 0.0 {
			perjob = totaldur / time.Duration(totaljobs)
		}
		fmt.Printf("   Pipelines: %d (avg duration %s)\n", totalpipelines, perpipeline)
		fmt.Printf("   Jobs: %d (avg duration %s)\n", totaljobs, perjob)

		grouppipelines += totalpipelines
		groupjobs += totaljobs
		groupdur += totaldur

		if false {
			for pipeline, pipeinfo := range info.Pipelines {
				pipedur := time.Duration(0.0)
				for _, duration := range pipeinfo.Jobs {
					pipedur += time.Second * time.Duration(duration)
				}
				fmt.Printf("      Pipeline %d %s\n", pipeline, pipedur)
				for job, duration := range pipeinfo.Jobs {
					fmt.Printf("        Job %d %s\n", job, time.Duration(duration)*time.Second)
				}

			}
		}
	}

	fmt.Printf("\n")
	fmt.Printf("Group %s\n", group)
	fmt.Printf(" Duration: %s\n", groupdur)
	groupperpipeline := time.Duration(0.0)
	groupperjob := time.Duration(0.0)
	if grouppipelines != 0 {
		groupperpipeline = groupdur / time.Duration(grouppipelines)
	}
	if groupjobs != 0 {
		groupperjob = groupdur / time.Duration(groupjobs)
	}
	fmt.Printf(" Pipelines: %d (avg duration %s)\n", grouppipelines, groupperpipeline)
	fmt.Printf(" Jobs: %d (avg duration %s)\n", groupjobs, groupperjob)
}
